import org.junit.Test;

public class MyTests {

	@Test
	public void helloWorldTest(){
		assert Main.helloWorld().equals("Hello world");
	}

	@Test
	public void fibbonaciTest() {
		assert Main.fibbonaci(0) == 0;
		assert Main.fibbonaci(1) == 1;
		assert Main.fibbonaci(3) == 2;
	}
}
